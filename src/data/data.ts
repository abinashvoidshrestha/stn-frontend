import {
  IChat,
  IComment,
  ICompany,
  IImage,
  IJob,
  INotification,
  IPost,
  IUser,
} from 'types';

export const avatar: IImage = {
  src: 'https://i.pravatar.cc',
  alt: 'dummy avatar',
};

export const image: IImage = {
  src: 'https://source.unsplash.com/random',
  alt: 'cover img',
};

export const users: IUser[] = [
  {
    id: '1',
    name: 'John Doe',
    headline: 'Software Developer at XYZ corp',
    followerCount: 100,
    avatar: avatar,
    location: 'kathmandu, nepal',
  },
];

export const companies: ICompany[] = [
  {
    id: '1',
    name: 'XYZ corp',
    category: 'Software Development',
    description: 'We develop scalable web apps',
    location: 'kathmandu, nepal',
    followers: 1000,
    employeeCount: 100,
    logo: avatar,
  },
];

export const jobs: IJob[] = [
  {
    id: '1',
    title: 'Software Developer for hire',
    description: 'Java Application Developer',
    applicantCount: 100,
    workplaceType: 'ON_SITE',
    type: 'FULL_TIME',
    category: 'Software Development',
    level: 'Junior Developer',
    date: Date.now().toString(),
    body: '<p>must know how to develop appilcation in Java</p>',
    isActiveRecruit: true,
    isAvailable: true,
    hiringTeam: users[0],
    company: companies[0],
  },
];

export const posts: IPost[] = [
  {
    id: '1',
    heading: 'how to create an html form',
    body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    bodyImg: image,
    date: Date.now().toString(),
    author: users[0],
    likeCount: 10,
    shareCount: 2,
  },
];

export const comments: IComment[] = [
  {
    id: '1',
    body: 'wow this is really intresting',
    date: Date.now().toString(),
    author: users[0],
  },
];

export const notifications: INotification[] = [
  {
    id: '1',
    body: 'News about new Software Development jobs',
    type: 'INFORMATION',
    isRead: false,
    isNew: true,
  },
];

export const chats: IChat[] = [
  {
    id: '1',
    user: users[0],
    body: 'Hello, how are you today',
    date: Date.now().toString(),
    isSeen: false,
  },
];
