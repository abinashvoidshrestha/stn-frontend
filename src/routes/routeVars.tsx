export const routeVars = {
  FEED: '/',
  SEARCH: '/search',
  JOB_DESCRIPTION: '/job-description',
  JOBS: '/jobs',
  CHAT: '/chat',
  NOTIFICATIONS: '/notifications',
  CONNECTIONS: '/connections',
  USER: '/user',
  LOGIN: '/login',
  REGISTER: '/register',
  COMPANY: '/company',
  ABOUT_US: '/about-us',
};
