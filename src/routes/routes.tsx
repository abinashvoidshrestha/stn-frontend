import { RouteObject } from 'react-router-dom';

import { Chat } from 'pages/Chat/Chat';
import { CompanyProfile } from 'pages/CompanyProfile/CompanyProfile';
import { Connections } from 'pages/Connections/Connections';
import { Feed } from 'pages/Feed/Feed';
import { JobDescription } from 'pages/JobDescription/JobDescription';
import { Jobs } from 'pages/Jobs/Jobs';
import { Login } from 'pages/Login/Login';
import NotFound from 'pages/NotFound/NotFound';
import { Notifications } from 'pages/Notifications/Notifications';
import { Register } from 'pages/Register/Register';
import { Search } from 'pages/Search/Search';
import { UserProfile } from 'pages/UserProfile/UserProfile';

import { Layout } from 'components/Layout/Layout';

import { routeVars } from './routeVars';

export const routesNoLayout: RouteObject[] = [
  {
    path: '/',
    children: [
      {
        path: routeVars.LOGIN,
        element: <Login />,
      },
      {
        path: routeVars.REGISTER,
        element: <Register />,
      },
    ],
  },
];

export const routesClientLayout: RouteObject[] = [
  {
    path: '/',
    element: <Layout />,
    children: [
      {
        index: true,
        path: routeVars.FEED,
        element: <Feed />,
      },
      {
        path: routeVars.SEARCH,
        element: <Search />,
      },
      {
        path: routeVars.JOB_DESCRIPTION,
        element: <JobDescription />,
      },
      {
        path: routeVars.JOBS,
        element: <Jobs />,
      },
      {
        path: routeVars.CHAT,
        element: <Chat />,
      },
      {
        path: routeVars.NOTIFICATIONS,
        element: <Notifications />,
      },
      {
        path: routeVars.CONNECTIONS,
        element: <Connections />,
      },
      {
        path: routeVars.USER,
        element: <UserProfile />,
      },
      {
        path: routeVars.COMPANY,
        element: <CompanyProfile />,
      },
      {
        path: routeVars.ABOUT_US,
        element: 'About Us',
      },
      {
        path: '*',
        element: <NotFound />,
      },
    ],
  },
];
