export interface IImage {
  src: string;
  alt: string;
}

export interface ICompany {
  id: string;
  name: string;
  category: string; // TODO: CompanyCategory separate
  description: string;
  location: string;
  followers: number;
  employeeCount: number;
  logo: IImage;
}

export const JobTypes = [
  'FULL_TIME',
  'PART_TIME',
  'CONTRACT',
  'TEMPORARY',
  'VOLUNTEERING',
  'INTERNSHIP',
] as const; // TS3.4 syntax
type JobType = typeof JobTypes[number]; // 'a'|'b'|'c';

export const WorkplaceTypes = ['ON_SITE', 'REMOTE', 'HYBRID'] as const; // TS3.4 syntax
type WorkplaceType = typeof WorkplaceTypes[number]; // 'a'|'b'|'c';

export interface IJob {
  id: string;
  title: string;
  description?: string;
  applicantCount?: number;
  workplaceType: WorkplaceType;
  type?: JobType;
  category?: string; // TODO: jobCategory separate SOFTWARE_ENGINEER, QA
  level?: string; // TODO: jobLevel separate SENIOR, JUNIOR
  date?: string;
  body?: any;
  isActiveRecruit: boolean;
  isAvailable: boolean;
  hiringTeam: IUser;
  company: ICompany;
}

export interface IUser {
  id: string;
  name: string;
  headline?: string;
  followerCount: number;
  avatar?: IImage;
  location?: string;
}

export interface IPost {
  id: string;
  heading: string;
  body?: string;
  bodyImg?: IImage;
  likeCount: number;
  shareCount: number;
  date: string;
  author: IUser;
}

const NotificationTypes = [
  'USER',
  'COMPANY',
  'INFORMATION',
  'JOB_RECOMMENDATION',
] as const; // TS3.4 syntax
type NotificationType = typeof NotificationTypes[number]; // 'a'|'b'|'c';

export interface INotification {
  id: string;
  body: string;
  type: NotificationType;
  isRead: boolean;
  isNew: boolean;
  img?: IImage;
}

export interface IChat {
  id: string;
  body: string;
  date: string;
  isSeen: boolean;
  user: IUser;
}

export interface IComment {
  id: string;
  body?: string;
  bodyImg?: IImage;
  author: IUser;
  date: string;
}
