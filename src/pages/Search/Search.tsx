import { Box } from '@mui/material';

import { Filter } from './Filter/Filter';
import { TabPanelCompany } from './Tabs/TabPanelCompany/TabPanelCompany';
import { TabPanelJob } from './Tabs/TabPanelJob/TabPanelJob';
import { TabPanelPeople } from './Tabs/TabPanelPeople/TabPanelPeople';

import BusinessOutlinedIcon from '@mui/icons-material/BusinessOutlined';
import FeedOutlinedIcon from '@mui/icons-material/FeedOutlined';
import PeopleAltOutlinedIcon from '@mui/icons-material/PeopleAltOutlined';
import WorkOutlineOutlinedIcon from '@mui/icons-material/WorkOutlineOutlined';
import { TabSwitcher } from 'components/UI/TabSwitcher/TabSwitcher';

export const Search = () => {
  const searchTabs = [
    {
      label: 'People',
      tab: <TabPanelPeople />,
      labelIcon: <PeopleAltOutlinedIcon />,
    },
    {
      label: 'Companies',
      tab: <TabPanelCompany />,
      labelIcon: <BusinessOutlinedIcon />,
    },
    {
      label: 'Jobs',
      tab: <TabPanelJob />,
      labelIcon: <WorkOutlineOutlinedIcon />,
    },
    { label: 'Posts', tab: 'Not Done', labelIcon: <FeedOutlinedIcon /> },
  ];

  return (
    <Box sx={{ display: 'flex', gap: 2 }}>
      <Filter />
      <Box sx={{ flex: 1, display: 'flex', flexDirection: 'column', gap: 2 }}>
        <TabSwitcher tabs={searchTabs} centered />
      </Box>
    </Box>
  );
};
