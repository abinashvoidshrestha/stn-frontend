import { CheckBox } from '@mui/icons-material';
import { Box, Divider, Paper, Theme, Typography } from '@mui/material';
import { Stack } from '@mui/system';
import { useState } from 'react';

interface IFilter {
  datePosted?: string;
  postedBy?: string;
  sortBy?: string;
}

const datePosted = ['24 hour', 'past week', 'past month'];
const postedBy = ['1st connection', 'me'];
const sortBy = ['top match', 'latest'];

export const Filter = () => {
  const [filters, setFilters] = useState<IFilter>({
    datePosted: undefined,
    postedBy: undefined,
    sortBy: undefined,
  });

  return (
    <Paper
      sx={(theme: Theme) => ({
        width: '22rem',
        px: 2,
        pt: 1,
        pb: 2,
        height: 'fit-content',
      })}
    >
      <Box sx={{ mb: 2 }}>
        <Typography fontWeight={'bold'} fontSize={'1.2rem'}>
          Filters
        </Typography>
      </Box>
      <Box>
        <Box>
          <Box sx={{ mb: 1 }}>
            <Typography fontWeight={'bold'}>Date Posted</Typography>
          </Box>
          <Box sx={{ mb: 1 }}>
            <Stack spacing={1}>
              {datePosted.map((item, idx) => (
                <Box key={idx} sx={{ display: 'flex', gap: 1 }}>
                  <CheckBox />
                  <Typography>{item}</Typography>
                </Box>
              ))}
            </Stack>
          </Box>
        </Box>
        <Divider sx={{ my: 2 }} />
        <Box>
          <Box sx={{ mb: 1 }}>
            <Typography fontWeight={'bold'}>Posted By</Typography>
          </Box>
          <Box sx={{ mb: 1 }}>
            <Stack spacing={1}>
              {postedBy.map((item, idx) => (
                <Box key={idx} sx={{ display: 'flex', gap: 1 }}>
                  <CheckBox />
                  <Typography>{item}</Typography>
                </Box>
              ))}
            </Stack>
          </Box>
        </Box>
        <Divider sx={{ my: 2 }} />
        <Box>
          <Box sx={{ mb: 1 }}>
            <Typography fontWeight={'bold'}>Sort by</Typography>
          </Box>
          <Box sx={{ mb: 1 }}>
            <Stack spacing={1}>
              {sortBy.map((item, idx) => (
                <Box key={idx} sx={{ display: 'flex', gap: 1 }}>
                  <CheckBox />
                  <Typography>{item}</Typography>
                </Box>
              ))}
            </Stack>
          </Box>
        </Box>
      </Box>
    </Paper>
  );
};
