import { Box, Pagination, Stack } from '@mui/material';
import React, { useEffect, useState } from 'react';

import { JobCard } from 'components/ContentCards/JobCard/JobCard';

import { jobs as jobData } from 'data/data';
import { IJob } from 'types';

export const TabPanelJob = () => {
  const [jobs, setJobs] = useState<IJob[]>([]);

  const PAGE_SIZE = 11;
  const [page, setPage] = React.useState(1);
  const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setPage(value);
  };

  useEffect(() => {
    const fetchJob = async () => {
      setJobs(jobData);
    };

    fetchJob();
  }, []);

  return (
    <Box>
      <Stack spacing={1} sx={{ px: 2, mb: 2, mt: 2 }}>
        {jobs
          .slice((page - 1) * PAGE_SIZE, page * PAGE_SIZE)
          .map((item, idx) => (
            <Box key={item.id}>
              <JobCard key={item.id} job={item} />
            </Box>
          ))}
      </Stack>

      {jobs.length > PAGE_SIZE && (
        <Box sx={{ pb: 2, display: 'flex', justifyContent: 'center' }}>
          <Pagination
            count={Math.round(jobs.length / PAGE_SIZE)}
            page={page}
            onChange={handleChange}
          />
        </Box>
      )}
    </Box>
  );
};
