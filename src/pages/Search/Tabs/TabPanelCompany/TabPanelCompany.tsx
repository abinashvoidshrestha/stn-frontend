import { Box, Divider, Pagination, Stack } from '@mui/material';
import React, { useEffect, useState } from 'react';

import { CompanyCard } from 'components/ContentCards/CompanyCard/CompanyCard';
import { ICompany } from 'types';

import { companies as companyData } from 'data/data';

export const TabPanelCompany = () => {
  const [companies, setCompanies] = useState<ICompany[]>([]);

  const PAGE_SIZE = 11;
  const [page, setPage] = React.useState(1);
  const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setPage(value);
  };

  useEffect(() => {
    const fetchJob = async () => {
      setCompanies(companyData);
    };

    fetchJob();
  }, []);

  return (
    <Box>
      <Stack spacing={1} sx={{ px: 2, mb: 2, mt: 2 }}>
        {companies
          .slice((page - 1) * PAGE_SIZE, page * PAGE_SIZE)
          .map((item, idx) => (
            <Box key={item.id}>
              <CompanyCard key={item.id} company={item} />
              {idx < companies.length - 1 && <Divider sx={{ my: 1 }} />}
            </Box>
          ))}
      </Stack>
      {companies.length > PAGE_SIZE && (
        <Box sx={{ pb: 2, display: 'flex', justifyContent: 'center' }}>
          <Pagination
            count={Math.round(companies.length / PAGE_SIZE)}
            page={page}
            onChange={handleChange}
          />
        </Box>
      )}
    </Box>
  );
};
