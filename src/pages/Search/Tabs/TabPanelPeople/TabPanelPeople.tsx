import { Box, Grid, Pagination } from '@mui/material';
import React, { useEffect, useState } from 'react';

import { PeopleCard } from 'components/ContentCards/PeopleCard/PeopleCard';

import { users as usersData } from 'data/data';
import { IUser } from 'types';

export const TabPanelPeople = () => {
  const [users, setUsers] = useState<IUser[]>([]);

  const PAGE_SIZE = 6;
  const [page, setPage] = React.useState(1);
  const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setPage(value);
  };

  useEffect(() => {
    const fetchJob = async () => {
      // TODO: fetch according to type
      setUsers(usersData);
    };

    fetchJob();
  }, []);

  return (
    <Box>
      <Grid container spacing={2} sx={{ mb: 2 }}>
        {users
          .slice((page - 1) * PAGE_SIZE, page * PAGE_SIZE)
          .map((item, idx) => (
            <Grid item key={item.id}>
              <PeopleCard key={item.id} user={item} />
            </Grid>
          ))}
      </Grid>
      {users.length > PAGE_SIZE && (
        <Box sx={{ pb: 2, display: 'flex', justifyContent: 'center' }}>
          <Pagination
            count={Math.round(users.length / PAGE_SIZE)}
            page={page}
            onChange={handleChange}
          />
        </Box>
      )}
    </Box>
  );
};
