import {
  Avatar,
  Box,
  Button,
  Divider,
  Paper,
  Stack,
  Typography
} from '@mui/material';

import BusinessOutlinedIcon from '@mui/icons-material/BusinessOutlined';
import WorkOutlineOutlinedIcon from '@mui/icons-material/WorkOutlineOutlined';

const job = {
  id: 1,
  title: 'Communications Engineer',
  description: 'Build stable communication network across galaxy',
  date: '3 days ago',
  applicantCount: 10,
  onSite: true,
  jobType: 'fulltime',
  jobBody: (
    <p>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat adipisci
      qui consectetur aut facilis pariatur delectus maiores explicabo similique
      modi aliquid totam, non voluptates quos corporis, tenetur nihil, animi
      reprehenderit ut! Repudiandae libero atque culpa quis velit repellat sed
      voluptate, nostrum nemo dolor optio deserunt, similique natus nisi,
      consequatur ad tempora rerum. Itaque earum pariatur molestiae ex. Natus
      optio excepturi quam alias, omnis rerum nam amet nemo voluptas delectus
      repudiandae quidem tempore tenetur quasi ullam nobis voluptatum hic
      dolore! Enim libero id obcaecati vel accusamus aspernatur, repudiandae
      fugit incidunt architecto culpa quas? Ipsum iste voluptatibus quos
      necessitatibus aliquid explicabo sit perspiciatis aperiam dignissimos,
      voluptas eaque a, commodi id accusamus, ullam quidem blanditiis deleniti
      pariatur labore suscipit. Ad est temporibus delectus quam nemo aspernatur.
      Ullam, tenetur tempore veritatis sint natus quos distinctio. Blanditiis
      quasi porro ea quam, obcaecati eligendi ad nobis, voluptatibus neque qui
      fugiat doloribus ipsa doloremque perspiciatis reiciendis illo amet quod
      quae fugit. Esse id possimus sint cupiditate! Suscipit nisi accusantium
      voluptatibus dicta ipsa explicabo iusto ducimus exercitationem ratione
      dolorum non, voluptatem aperiam, magni esse est. Voluptate exercitationem
      similique error! Autem ipsa enim necessitatibus ut iste expedita iure
      explicabo alias? Excepturi nesciunt cupiditate perspiciatis magni culpa,
      nostrum obcaecati, quasi itaque aspernatur quos rem dolorum consectetur
      repellendus laborum eum, doloremque doloribus animi? Commodi ipsum
      expedita blanditiis dolorum, voluptatibus iste iusto! Explicabo
      voluptatibus eius reiciendis numquam error repellendus earum vero officia!
      Fuga consectetur ducimus, numquam fugit velit incidunt ullam laboriosam
      libero obcaecati quam sequi delectus repudiandae tempore provident
      voluptate repellendus corrupti sed quis nemo minima necessitatibus
      possimus voluptatum enim culpa? Suscipit nostrum dignissimos minus
      reiciendis ad dolorem doloremque reprehenderit perferendis temporibus
      molestiae dolores porro est itaque consectetur impedit, consequuntur
      adipisci velit consequatur autem tempora voluptatum placeat a accusantium?
      Delectus dolorum voluptates nesciunt eos praesentium? Autem, a ullam?
      Aspernatur tempora incidunt nobis vitae reprehenderit error quis
      blanditiis voluptates tenetur eius suscipit, minima velit quia praesentium
      harum earum facilis. Quis molestias sed quo doloribus possimus, doloremque
      repellendus optio eius impedit fugiat adipisci porro earum ipsa excepturi
      similique. Suscipit tempora tempore ipsum fugit quisquam deleniti qui!
      Soluta nemo necessitatibus labore accusantium vitae, neque, aliquid aut
      provident possimus reprehenderit praesentium voluptas porro, temporibus
      veritatis quod expedita dolorum vero officia nulla nostrum dolores
      corporis magni pariatur repellendus! Aliquid voluptatem aut magni! Laborum
      accusamus alias fuga facilis quod a dicta dolores. Animi hic qui molestiae
      iste aliquid porro odit vitae quasi eveniet nemo nam vero laudantium, non
      sunt, quae quo? Nobis aliquid cupiditate quam fuga inventore, deleniti
      excepturi quod perspiciatis voluptatum, ad, illo iste consectetur autem
      corrupti atque dolores expedita quaerat. Itaque quis iusto qui? Unde enim
      eaque soluta doloremque. Voluptatum itaque distinctio officia ut
      asperiores id fugiat dolorum enim, consequatur aliquid iure illum, earum
      odio quae temporibus iste vitae. Explicabo, iusto alias ullam unde
      deleniti ipsum reprehenderit adipisci fuga necessitatibus, atque qui esse
      asperiores laborum porro ducimus, dolorem sed dolor quas vero saepe. Iure
      deserunt iste odio voluptate inventore impedit molestiae accusantium
      consequuntur non enim corporis porro, nulla placeat facere perspiciatis
      odit temporibus repellendus? Qui, eveniet hic ipsam quod repudiandae sequi
      nostrum doloribus? Accusamus enim velit id est excepturi magnam, debitis
      maxime sint obcaecati odit doloremque soluta repellat. Est necessitatibus
      quas corrupti saepe ipsum! Modi exercitationem repellat corporis eveniet
      veritatis aperiam aliquid fugiat possimus cupiditate et, quisquam officiis
      aut autem fugit doloremque voluptas voluptatem atque suscipit nemo quasi
      nulla dolorum temporibus maiores quae! Omnis vero cupiditate esse?
      Possimus, tempora? Odio minus maiores, officiis distinctio unde aut quae
      perspiciatis iusto quisquam numquam assumenda et nesciunt tempora
      similique voluptate aliquid nihil placeat a beatae blanditiis dolore
      magnam! Aspernatur rerum, porro quibusdam numquam atque magnam! Fuga eos
      voluptas obcaecati, repellat voluptatibus quasi quis eum aut illum ipsam
      animi, voluptatum veniam harum totam fugit ut aliquam hic quisquam quam.
      Totam, ex! Quae quos quam quia sunt deserunt! Eos cumque enim accusamus
      harum nulla sequi, fugit fugiat ad. Dolorem sequi adipisci, eaque hic
      eveniet illum impedit nihil repudiandae dolore minus quis corrupti.
      Dolorum eum maxime voluptate eius dolores accusantium cupiditate.
      Asperiores voluptatibus esse iste eum explicabo nesciunt provident enim
      optio quam officiis fuga quibusdam natus, laborum pariatur commodi, quis
      eos debitis nemo nihil, unde est nam. Officiis vitae rerum eos molestiae
      corrupti laboriosam nam voluptas iure itaque! Ex animi exercitationem
      illum esse molestias deserunt, nesciunt eos laborum odio perspiciatis quia
      veniam delectus ipsam a quisquam debitis nam tenetur accusantium dolorum?
      Accusantium minima reiciendis velit aliquid magni! Soluta quam nostrum
      eligendi accusamus. Et ad incidunt rem veritatis, vel dolorum expedita
      iste sunt eum officia a ullam laboriosam placeat id quidem deserunt
      debitis. Reiciendis totam incidunt quisquam dolorum deleniti perspiciatis
      perferendis ipsam ratione, praesentium quaerat necessitatibus, tenetur
      saepe inventore autem voluptatibus voluptate, est modi suscipit corrupti
      vitae beatae explicabo! Eos ipsam quis modi nam eligendi quibusdam eum
      omnis saepe vitae distinctio officia cupiditate mollitia voluptate
      voluptates incidunt aperiam et praesentium inventore, deleniti tempore
      sapiente fugit aut iure doloremque. Quam possimus iusto accusamus cumque
      blanditiis nesciunt neque tempore, deleniti sunt fugit? Recusandae dolores
      nulla ea illum esse quae repudiandae, beatae qui soluta ducimus totam
      perferendis, magni vitae explicabo? Nulla asperiores, voluptas quis
      inventore molestiae in consequatur dolore, sint excepturi assumenda,
      dignissimos doloremque voluptate omnis vitae sunt ipsam tempore quae modi
      tempora reiciendis possimus autem. Qui provident doloribus minima possimus
      cumque debitis quam earum eos recusandae sint assumenda quos veniam optio,
      distinctio eligendi, voluptatem animi esse obcaecati asperiores nemo eius,
      pariatur cupiditate. Dolorum, ab consequatur ipsa enim ratione est
      quisquam quos. Doloribus, alias nulla deserunt unde, laborum id quia
      praesentium aperiam et est esse pariatur quidem nisi quaerat nobis
      similique eligendi eveniet dignissimos quae quo perferendis! Sunt itaque
      atque ab facere delectus unde. Iusto id ducimus officiis culpa placeat
      nihil, provident adipisci perferendis blanditiis quisquam aut, nobis est,
      veniam ipsam ab non reiciendis laboriosam praesentium eos maiores nostrum
      excepturi molestiae? Eum ullam dolor hic ipsa dolores repellendus possimus
      soluta praesentium nulla modi! Quia nostrum quos omnis corporis ex odit
      incidunt expedita consequuntur eligendi nesciunt? Magnam sequi fugiat ab
      nobis voluptatibus dignissimos commodi. Ducimus qui, libero velit possimus
      labore harum similique, itaque magnam aliquam expedita iste!
    </p>
  ),
  hiringTeam: {
    name: 'Bilbo Baggins',
    headline: 'Ring Bearer at Middle Earth',
    followerCount: 100,
    avatar: {
      src: 'https://i.pravatar.cc',
      alt: 'dummy avatar',
    },
  },
  company: {
    id: 1,
    location: 'Kathmandu, Nepal',
    name: 'Umbrella Corp',
    followers: 1000,
    employeeCount: 10000,
    description: 'We make scarey ass zombies',
    logo: {
      src: 'https://i.pravatar.cc',
      alt: 'dummy avatar',
    },
  },
};

export const JobDescriptionView = () => {
  return (
    <Paper sx={{ flex: 1, overflowY: 'scroll' }}>
      <Box sx={{ px: 3, pt: 3, mb: 2 }}>
        <Typography fontSize={'1.4rem'} fontWeight={'bold'}>
          {job.title}
        </Typography>
        <Typography fontSize={'1rem'} sx={{ display: 'inline-flex', mr: 1 }}>
          {job.company.name},
        </Typography>
        <Typography fontSize={'1rem'} sx={{ display: 'inline-flex' }}>
          {job.company.location}
        </Typography>
        <Typography fontSize={'1rem'} color={'secondary'}>
          {job.applicantCount} applicants
        </Typography>
      </Box>
      <Stack sx={{ px: 3 }} spacing={1}>
        <Box sx={{ display: 'flex', gap: 2 }}>
          <WorkOutlineOutlinedIcon />
          <Typography fontSize={'1rem'} textTransform={'capitalize'}>
            {job.jobType}
          </Typography>
        </Box>
        <Box sx={{ display: 'flex', gap: 2 }}>
          <BusinessOutlinedIcon />
          <Typography fontSize={'1rem'}>
            {job.company.employeeCount} employees
          </Typography>
        </Box>
        <Box sx={{ display: 'flex', gap: 2, mt: 1, alignItems: 'center' }}>
          <Avatar
            src={job.hiringTeam.avatar.src}
            alt={job.hiringTeam.avatar.alt}
            sx={{
              height: '2.2rem',
              width: '2.2rem',
            }}
          >
            {job.hiringTeam.name.split('')[0]}
          </Avatar>
          <Typography fontSize={'1rem'}>
            {job.hiringTeam.name} is hiring for this job
          </Typography>
        </Box>
      </Stack>
      <Box sx={{ px: 3, mt: 2, mb: 3, display: 'flex', gap: 2 }}>
        <Button variant={'contained'}>Apply for Job</Button>
        <Button variant={'outlined'}>Save</Button>
      </Box>
      <Divider sx={{ mx: 3 }} />
      <Box sx={{ px: 3, pt: 1 }}>{job.jobBody}</Box>
    </Paper>
  );
};
