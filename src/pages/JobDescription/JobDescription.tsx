import { Box } from '@mui/material';

import { JobDescriptionView } from './JobDescriptionView/JobDescriptionView';
import { JobList } from './JobList/JobList';

export const JobDescription = () => {
  return (
    <Box sx={{ display: 'flex', gap: 2, height: '90vh' }}>
      <JobList />
      <JobDescriptionView />
    </Box>
  );
};
