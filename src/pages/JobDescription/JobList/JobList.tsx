import {
  Box,
  Divider,
  Pagination,
  Paper,
  Stack,
  Typography,
} from '@mui/material';
import React, { useEffect, useState } from 'react';

import { JobCard } from 'components/ContentCards/JobCard/JobCard';

import { jobs as jobData } from 'data/data';
import { IJob } from 'types';

export const JobList = () => {
  const [jobs, setJobs] = useState<IJob[]>([]);

  const PAGE_SIZE = 11;
  const [page, setPage] = React.useState(1);
  const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setPage(value);
  };

  useEffect(() => {
    const fetchJob = async () => {
      setJobs(jobData);
    };

    fetchJob();
  }, []);

  return (
    <Paper sx={{ width: '24rem', minWidth: '24rem' }}>
      <Box sx={{ px: 2, pt: 2, mb: 1 }}>
        <Typography fontWeight={'bold'} fontSize={'1.2rem'}>
          Jobs For You
        </Typography>
      </Box>
      <Divider />
      <Box
        sx={{
          overflowY: 'scroll',
          height: '84vh',
        }}
      >
        <Stack spacing={1} sx={{ pt: 1, px: 1, mb: 2 }}>
          {jobs.map((item, idx) => (
            <Box key={item.id}>
              <JobCard key={item.id} job={item} />
              {idx < jobs.length - 1 && <Divider sx={{ my: 1 }} />}
            </Box>
          ))}
        </Stack>
        {jobs.length > PAGE_SIZE && (
          <Box sx={{ pb: 2, display: 'flex', justifyContent: 'center' }}>
            <Pagination
              count={Math.round(jobs.length / PAGE_SIZE)}
              page={page}
              onChange={handleChange}
            />
          </Box>
        )}
      </Box>
    </Paper>
  );
};
