import { Box, Pagination, Stack, Theme } from '@mui/material';
import { JobCard } from 'components/ContentCards/JobCard/JobCard';
import React, { useEffect, useState } from 'react';
import { IJob } from 'types';

import { jobs as jobData } from 'data/data';

interface IJobTab {
  type: 'MY' | 'SAVED' | 'APPLIED';
}

export const JobTab: React.FC<IJobTab> = ({ type }) => {
  const [jobs, setJobs] = useState<IJob[]>([]);

  const PAGE_SIZE = 11;
  const [page, setPage] = React.useState(1);
  const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setPage(value);
  };

  useEffect(() => {
    const fetchJob = async () => {
      // TODO: fetch according to type
      setJobs(jobData);
    };

    fetchJob();
  }, []);

  return (
    <Box>
      <Stack spacing={1} sx={{ px: 2, mb: 2, mt: 2 }}>
        {jobs
          .slice((page - 1) * PAGE_SIZE, page * PAGE_SIZE)
          .map((item, idx) => (
            <Box
              key={item.id}
              sx={(theme: Theme) => ({
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-between',
                borderRadius: '4px',
                p: 2,
              })}
            >
              <Box sx={{ flex: 1 }}>
                <JobCard key={item.id} job={item} />
              </Box>
              {/* TODO: add actions */}
              <Box>Actions</Box>
            </Box>
          ))}
      </Stack>
      {jobs.length > PAGE_SIZE && (
        <Box sx={{ pb: 2, display: 'flex', justifyContent: 'center' }}>
          <Pagination
            count={Math.round(jobs.length / PAGE_SIZE)}
            page={page}
            onChange={handleChange}
          />
        </Box>
      )}
    </Box>
  );
};
