import { Box, Button, Paper, Typography } from '@mui/material';
import { useState } from 'react';

import CreateOutlinedIcon from '@mui/icons-material/CreateOutlined';

import { TabSwitcher } from '../../components/UI/TabSwitcher/TabSwitcher';
import { CreateJobDialog } from './CreateJobDialog/CreateJobDialog';
import { JobTab } from './JobTab/JobTab';

export const Jobs = () => {
  const [createDialogOpen, setCreateDialogOpen] = useState(false);

  const handleCreateDialogClickOpen = () => {
    setCreateDialogOpen(true);
  };

  const handleCreateDialogClose = () => {
    setCreateDialogOpen(false);
  };

  const jobTabs = [
    { label: 'My Jobs', tab: <JobTab type={'MY'} /> },
    { label: 'Applied Jobs', tab: <JobTab type={'APPLIED'} /> },
    { label: 'Saved Jobs', tab: <JobTab type={'SAVED'} /> },
  ];

  return (
    <Box sx={{ display: 'flex', gap: 2 }}>
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          flexDirection: 'column',
          gap: 2,
        }}
      >
        <Paper sx={{ width: '18rem', p: 2, textAlign: 'center' }}>
          <Typography fontSize={'1.2rem'} fontWeight={'bold'} sx={{ mb: 1 }}>
            Create a Free Job Now
          </Typography>
          <Button
            variant="outlined"
            startIcon={<CreateOutlinedIcon />}
            onClick={handleCreateDialogClickOpen}
          >
            Post a Job
          </Button>
          <CreateJobDialog open={createDialogOpen} onClose={handleCreateDialogClose} />
        </Paper>
        <Box></Box>
      </Box>
      <Box sx={{ flex: 1, display: 'flex', flexDirection: 'column', gap: 2 }}>
        <TabSwitcher tabs={jobTabs} centered />
      </Box>
    </Box>
  );
};
