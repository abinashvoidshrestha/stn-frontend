import { Box, Button, Dialog, DialogActions, DialogContent, DialogTitle, FormControl, Grid, IconButton, InputLabel, MenuItem, Select, TextField, Theme, Typography } from '@mui/material';
import { DatePicker } from '@mui/x-date-pickers';
import { useState } from 'react';

import CloseOutlinedIcon from '@mui/icons-material/CloseOutlined';
import { WYSIWYG } from 'components/UI/WYSIWYG/WYSIWYG';

interface ICreateJobDialog {
    open: boolean;
    onClose: () => void;
}

export const CreateJobDialog: React.FC<ICreateJobDialog> = ({ open, onClose }) => {
    const [dialogPage, setDialogPage] = useState(0)


    const handleDialogClose = () => {
        onClose()
        setTimeout(
            () => setDialogPage(0),
            500
        )
    }

    const handleNextAction = () => {
        if (dialogPage === dialogPages.length - 1) {
            // TODO: save
        } else {
            setDialogPage(prevDialogPage => prevDialogPage + 1)
        }
    }

    const dialogPages = [
        <Box component={'form'} sx={{ p: 1 }} onSubmit={() => { }} >
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <TextField
                        name='jobTitle'
                        id='jobTitle'
                        label='Job Title'
                        required
                        autoFocus
                        fullWidth
                    />
                </Grid>

                <Grid item xs={6}>
                    <DatePicker
                        label={"Last date of application"}
                        value={"2022/1/11"}
                        onChange={() => { }}
                        renderInput={(param) => <TextField {...param} />} />
                </Grid>
                <Grid item xs={6}>
                    <FormControl fullWidth>
                        <InputLabel id="companyNameLabel">Company Name</InputLabel>
                        <Select
                            labelId="companyNameLabel"
                            id="companyName"
                            value={"1"}
                            label="Company Name"
                            onChange={() => { }}
                        >
                            <MenuItem value={"1"}>XYZ corp</MenuItem>
                            <MenuItem value={"2"}>Umbrella corp</MenuItem>
                            <MenuItem value={"3"}>Emerald guild</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={6}>
                    <FormControl fullWidth>
                        <InputLabel id="categoryLabel">Category</InputLabel>
                        <Select
                            labelId="categoryLabel"
                            id="category"
                            value={"1"}
                            label="Category"
                            onChange={() => { }}
                        >
                            <MenuItem value={"1"}>Software Development</MenuItem>
                            <MenuItem value={"2"}>Bio Tech</MenuItem>
                            <MenuItem value={"3"}>Merchant</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={6}>
                    <FormControl fullWidth>
                        <InputLabel id="workplaceTypeLabel">Workplace Type</InputLabel>
                        <Select
                            labelId="workplaceTypeLabel"
                            id="workplaceType"
                            value={"1"}
                            label="Workplace Type"
                            onChange={() => { }}
                        >
                            <MenuItem value={"1"}>Full Time</MenuItem>
                            <MenuItem value={"2"}>Part Time</MenuItem>
                            <MenuItem value={"3"}>Contract</MenuItem>
                            <MenuItem value={"3"}>Temporary</MenuItem>
                            <MenuItem value={"3"}>Volunteering</MenuItem>
                            <MenuItem value={"3"}>Internship</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={6}>
                    <TextField
                        name='jobLevel'
                        id='jobLevel'
                        label='Job Level'
                        fullWidth
                    />
                </Grid>
            </Grid>
        </Box >
        ,
        <Box component={'form'} sx={{ p: 1 }} onSubmit={() => { }} >
            <WYSIWYG />
        </Box>
    ]

    return (
        <Dialog
            open={open}
            onClose={handleDialogClose}
            sx={(theme: Theme) => ({
                '& .MuiPaper-root': {
                    position: 'absolute',
                    top: '3.5rem',
                },
                '& .MuiDialogContent-root': {
                    padding: theme.spacing(1),
                },
                '& .MuiDialogActions-root': {
                    padding: theme.spacing(2),
                },
            })}
        >
            <DialogTitle
                sx={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                }}
            >
                <Typography fontSize={'1.2rem'}>Find the right person for the Job</Typography>
                <IconButton
                    onClick={handleDialogClose}
                    sx={{
                        borderRadius: '4px',
                        p: '6px',
                    }}
                >
                    <CloseOutlinedIcon />
                </IconButton>
            </DialogTitle>
            <DialogContent>
                {dialogPages[dialogPage]}
            </DialogContent>
            <DialogActions
            >
                <Button
                    disableElevation
                    variant="contained"
                    sx={{
                        textTransform: 'capitalize',
                    }}
                    onClick={handleNextAction}
                >
                    {dialogPage === dialogPages.length - 1 ? "Create" : "Next"}
                </Button>
            </DialogActions>
        </Dialog>
    )
}
