import { Avatar, Box, Paper, Theme, Typography } from '@mui/material';
import { Stack } from '@mui/system';
import { notifications } from 'data/data';

export const Notifications = () => {
  return (
    <Paper
      sx={{
        width: '70vh',
        margin: '0 auto', // TODO: only middle part vako bela
        p: 2,
      }}
    >
      <Stack spacing={2}>
        {notifications.map((notification, idx) => (
          <Box
            sx={(theme: Theme) => ({
              display: 'flex',
              gap: 2,
              border: `1px solid ${theme.palette.divider}`,
              borderRadius: '4px',
              p: 1,
              ...(notification.isNew && {
                backgroundColor: theme.palette.action.selected,
              }),
            })}
          >
            {notification.img && (
              <Avatar
                src={notification.img.src}
                alt={notification.img.alt}
                sx={{
                  height: '4.2rem',
                  width: '4.2rem',
                }}
              >
                {notification.img.alt.split('')[0]}
              </Avatar>
            )}
            <Box {...(notification.img ? { sx: { mt: 1 } } : { sx: { p: 1 } })}>
              <Typography fontSize={'0.9rem'}>{notification.body}</Typography>
            </Box>
          </Box>
        ))}
      </Stack>
    </Paper>
  );
};
