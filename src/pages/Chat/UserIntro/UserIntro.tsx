import { Avatar, Box, Theme, Typography } from '@mui/material';
import { IUser } from 'types';

interface IUserIntro {
  user: IUser;
}

export const UserIntro: React.FC<IUserIntro> = ({ user }) => {
  return (
    <Box
      sx={(theme: Theme) => ({
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        border: `1px solid ${theme.palette.divider}`,
        borderRadius: '4px',
        p: 2,
        textAlign: 'center',
      })}
    >
      <Avatar
        {...(user.avatar && {
          src: user.avatar.src,
          alt: user.avatar.alt,
        })}
        sx={{
          height: '5.2rem',
          width: '5.2rem',
        }}
      >
        {user.name.split('')[0]}
      </Avatar>
      <Box>
        <Typography fontSize={'1rem'} fontWeight={'bold'}>
          {user.name}
        </Typography>
        <Typography fontSize={'0.9rem'} color={'GrayText'}>
          {user.headline}
        </Typography>
      </Box>
    </Box>
  );
};
