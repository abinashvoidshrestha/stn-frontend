import { Box } from '@mui/material';
import { IChat } from 'types';
import { ChatBlock } from './ChatBlock/ChatBlock';
import { DateDivider } from './DateDivider/DateDivider';

interface IChatComponent {
  chats: IChat[];
}

// TODO: stack all the same user series of chat in 1 chat block
export const ChatComponent: React.FC<IChatComponent> = ({ chats }) => {
  return (
    <Box sx={{ py: 2 }}>
      <DateDivider date={'Yesterday'} />
      {chats.map((chat) => (
        <ChatBlock key={chat.id} chat={chat} />
      ))}
    </Box>
  );
};
