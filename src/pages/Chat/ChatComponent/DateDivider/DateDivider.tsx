import { Divider, Typography } from '@mui/material';

interface IDateDivider {
  date: string;
}

export const DateDivider: React.FC<IDateDivider> = ({ date }) => {
  return (
    <Divider>
      <Typography fontSize={'0.9rem'} color={'GrayText'}>
        {date}
      </Typography>
    </Divider>
  );
};
