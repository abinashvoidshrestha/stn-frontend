import { Avatar, Box, Stack, Theme, Typography } from '@mui/material';
import { IChat } from 'types';

interface IChatBlock {
  chat: IChat;
}

// TODO: seen? implement
export const ChatBlock: React.FC<IChatBlock> = ({ chat }) => {
  return (
    <Box
      sx={{
        py: 2,
        px: 2,
        borderRadius: '4px',
        display: 'flex',
        gap: 2,
      }}
    >
      <Avatar
        {...(chat.user.avatar && {
          src: chat.user.avatar.src,
          alt: chat.user.avatar.alt,
        })}
        sx={{
          height: '2.6rem',
          width: '2.6rem',
        }}
      >
        {chat.user.name.split('')[0]}
      </Avatar>
      <Box sx={{}}>
        <Typography fontSize={'0.9rem'} fontWeight={'bold'} sx={{ mb: 1 }}>
          {chat.user.name}
        </Typography>
        <Stack spacing={1}>
          <Box
            sx={(theme: Theme) => ({
              backgroundColor: theme.palette.grey[300],
              borderRadius: '4px',
              p: 2,
            })}
          >
            {chat.body}
          </Box>
        </Stack>
      </Box>
    </Box>
  );
};
