import { Box, Divider, Paper, Typography } from '@mui/material';
import { chats, users } from 'data/data';
import { ChatComponent } from './ChatComponent/ChatComponent';

import { ChatList } from './ChatList/ChatList';
import { UserIntro } from './UserIntro/UserIntro';

export const Chat = () => {
  const user = users[0];

  return (
    <Box sx={{ display: 'flex', gap: 2, height: '90vh' }}>
      <ChatList />
      <Paper sx={{ display: 'flex', width: '100%' }}>
        <Box sx={{ flex: 1 }}>
          <Box sx={{ px: 3, py: 2 }}>
            <Typography fontWeight={'bold'} fontSize={'1.2rem'}>
              {user.name}
            </Typography>
          </Box>
          <Divider />
          <Box
            sx={{
              p: 2,
              height: '83vh',
              overflowY: 'hidden',
              ':hover': {
                overflowY: 'scroll',
              },
            }}
          >
            <UserIntro user={user} />
            <ChatComponent chats={chats} />
          </Box>
        </Box>
      </Paper>
    </Box>
  );
};
