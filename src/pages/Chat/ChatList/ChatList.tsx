import {
  Avatar,
  Box,
  Divider,
  Paper,
  Stack,
  Theme,
  Typography,
} from '@mui/material';
import { users } from 'data/data';

export const ChatList = () => {
  const user = users[0];

  return (
    <Paper sx={{ display: 'flex' }}>
      <Box sx={(theme: Theme) => ({})}>
        <Box>
          <Box sx={{ px: 3, py: 2 }}>
            <Typography fontWeight={'bold'} fontSize={'1.2rem'}>
              Message
            </Typography>
          </Box>
          <Divider />
          <Box
            sx={{
              p: 1,
              width: '22rem',
              height: '84vh',
              overflowY: 'hidden',
              ':hover': {
                overflowY: 'scroll',
              },
            }}
          >
            <Stack spacing={1}>
              <Box
                sx={(theme: Theme) => ({
                  display: 'flex',
                  px: 1,
                  py: 2,
                  borderRadius: '4px',
                  alignItems: 'center',
                  gap: 2,
                  cursor: 'pointer',
                  ':hover': {
                    backgroundColor: theme.palette.action.hover,
                  },
                })}
              >
                <Avatar
                  {...(user.avatar && {
                    src: user.avatar.src,
                    alt: user.avatar.alt,
                  })}
                  sx={{
                    height: '3.2rem',
                    width: '3.2rem',
                  }}
                >
                  {user.name.split('')[0]}
                </Avatar>
                <Box>
                  <Typography fontSize={'1rem'} fontWeight={'bold'}>
                    {user.name}
                  </Typography>
                  <Typography fontSize={'0.9rem'} color={'GrayText'}>
                    {user.headline}
                  </Typography>
                </Box>
              </Box>
            </Stack>
          </Box>
        </Box>
      </Box>
    </Paper>
  );
};
