import { Avatar, Box, Paper, Stack, Theme, Typography } from '@mui/material';

const data = {
  name: 'Bilbo Baggins',
  headline: 'Ring Bearer at Middle Earth',
  location: 'Kathmandu, Nepal',
  followerCount: 100,
  avatar: {
    src: 'https://i.pravatar.cc',
    alt: 'dummy avatar',
  },
  coverImg: {
    src: 'https://picsum.photos/400/200',
    alt: 'cover img',
  },
};

// TODO: complete this
export const UserProfile = () => {
  return (
    <Paper
      sx={{
        position: 'relative',
        overflow: 'hidden',
      }}
    >
      <Box
        sx={{
          height: '12rem',
          overflow: 'hidden',
          '& img': {
            objectFit: 'cover',
            width: '100%',
          },
        }}
      >
        <img src={data.coverImg.src} alt={data.coverImg.alt} />
      </Box>
      <Avatar
        src={data.avatar.src}
        alt={data.avatar.alt}
        sx={(theme: Theme) => ({
          position: 'absolute',
          top: '5rem',
          mx: 4,
          height: '10rem',
          width: '10rem',
          border: `4px solid ${theme.palette.common.white}`,
        })}
      >
        {data.name.split('')[0]}
      </Avatar>
      <Box
        sx={{
          mt: '2rem',
          p: 3,
          display: 'flex',
          justifyContent: 'space-between',
        }}
      >
        <Box>
          <Typography fontSize={'1.4rem'} fontWeight={'bold'}>
            {data.name}
          </Typography>
          <Typography fontSize={'1rem'}>{data.headline} followers</Typography>
          <Typography fontSize={'0.9rem'} color={'GrayText'} sx={{ mt: '4px' }}>
            {data.location}
          </Typography>

          <Box sx={{ my: 1 }}>
            <Typography
              fontSize={'1rem'}
              fontWeight={'bold'}
              color={'GrayText'}
              sx={{ display: 'inline-flex' }}
            >
              {data.followerCount}
            </Typography>{' '}
            <Typography
              fontSize={'0.9rem'}
              color={'GrayText'}
              sx={{ display: 'inline-flex' }}
            >
              connections
            </Typography>
            <Box>mutual</Box>
          </Box>
        </Box>
        <Box>
          <Stack spacing={1}>
            <Box
              sx={(theme: Theme) => ({
                display: 'flex',
                alignItems: 'center',
                gap: 1,
                border: `1px solid ${theme.palette.divider}`,
                borderRadius: '4px',
                p: 1,
              })}
            >
              <Avatar
                src={data.avatar.src}
                alt={data.avatar.alt}
                sx={(theme: Theme) => ({
                  height: '2.5rem',
                  width: '2.5rem',
                  borderRadius: '4px',
                })}
              >
                {data.name.split('')[0]}
              </Avatar>
              <Typography
                fontSize={'0.9rem'}
                fontWeight={'bold'}
                color={'GrayText'}
                sx={{ display: 'inline-flex', mr: 1 }}
              >
                Lorem ipsum dolor sit.
              </Typography>
            </Box>
            <Box
              sx={(theme: Theme) => ({
                display: 'flex',
                alignItems: 'center',
                gap: 1,
                border: `1px solid ${theme.palette.divider}`,
                borderRadius: '4px',
                p: 1,
              })}
            >
              <Avatar
                src={data.avatar.src}
                alt={data.avatar.alt}
                sx={(theme: Theme) => ({
                  height: '2.5rem',
                  width: '2.5rem',
                  borderRadius: '4px',
                })}
              >
                {data.name.split('')[0]}
              </Avatar>
              <Typography
                fontSize={'0.9rem'}
                fontWeight={'bold'}
                color={'GrayText'}
                sx={{ display: 'inline-flex', mr: 1 }}
              >
                Lorem ipsum dolor sit.
              </Typography>
            </Box>
          </Stack>
        </Box>
      </Box>
    </Paper>
  );
};
