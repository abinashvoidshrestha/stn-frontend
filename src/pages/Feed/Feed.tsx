import { Box } from '@mui/material';

import { AddToFeedCard } from 'components/AddToFeedCard/AddToFeedCard';
import { UserSideCard } from 'components/UserSideCard/UserSideCard';
import { CreatePost } from './CreatePost/CreatePost';
import { Post } from './Post/Post';

import { image as coverImg, posts } from 'data/data';

export const Feed = () => {
  return (
    <Box sx={{ display: 'flex', justifyContent: 'center', gap: 3 }}>
      <Box>
        <UserSideCard user={posts[0].author} coverImg={coverImg} />
      </Box>
      <Box>
        <CreatePost user={posts[0].author} />
        <Box
          sx={{
            display: 'flex',
            alignItems: 'center',
            flexDirection: 'column',
            gap: 2,
          }}
        >
          {posts.map((post, idx) => (
            <Post key={post.id} post={post} />
          ))}
        </Box>
      </Box>
      <Box>
        <AddToFeedCard />
      </Box>
    </Box>
  );
};
