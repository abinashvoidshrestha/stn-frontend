import {
  Avatar,
  Box,
  Button, Paper, Theme,
  Typography
} from '@mui/material';
import React, { useState } from 'react';

import ArticleOutlinedIcon from '@mui/icons-material/ArticleOutlined';
import CalendarMonthOutlinedIcon from '@mui/icons-material/CalendarMonthOutlined';
import InsertPhotoOutlinedIcon from '@mui/icons-material/InsertPhotoOutlined';
import SmartDisplayOutlinedIcon from '@mui/icons-material/SmartDisplayOutlined';

import { IUser } from 'types';
import { CreatePostDialog } from './CretePostDialog/CreatePostDialog';

interface ICreatePost {
  user: IUser;
}

export const CreatePost: React.FC<ICreatePost> = ({ user }) => {
  const [dialogOpen, setDialogOpen] = useState(false);

  const handleDialogClickOpen = () => {
    setDialogOpen(true);
  };

  const handleDialogClose = () => {
    setDialogOpen(false);
  };

  return (
    <Paper sx={{ mb: 2, px: 2, py: 1 }}>
      <Box sx={{ display: 'flex', alignItems: 'center' }}>
        <Avatar
          {...(user.avatar && {
            src: user.avatar.src,
            alt: user.avatar.alt,
          })}
          sx={{
            height: '3.2rem',
            width: '3.2rem',
          }}
        >
          {user.name.split('')[0]}
        </Avatar>
        <Button
          onClick={handleDialogClickOpen}
          sx={(theme: Theme) => ({
            backgroundColor: theme.palette.grey[100],
            width: '100%',
            height: '2.6rem',
            borderRadius: '4px',
            display: 'flex',
            alignItems: 'center',
            textTransform: 'none',
            justifyContent: 'flex-start',
            ml: 1,
            px: 2,
            cursor: 'text',
            ':hover': {
              backgroundColor: theme.palette.grey[200],
            },
          })}
        >
          <Typography color={'GrayText'}>Start a post</Typography>
        </Button>
        <CreatePostDialog open={dialogOpen} onClose={handleDialogClose} />
      </Box>
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          mt: 1,
        }}
      >
        <Button
          sx={(theme: Theme) => ({
            textTransform: 'capitalize',
            color: theme.palette.grey[600],
          })}
        >
          <InsertPhotoOutlinedIcon sx={{ fontSize: '1.5rem', mr: '6px' }} />{' '}
          Photo
        </Button>

        <Button
          sx={(theme: Theme) => ({
            textTransform: 'capitalize',
            color: theme.palette.grey[600],
          })}
        >
          <SmartDisplayOutlinedIcon sx={{ fontSize: '1.5rem', mr: '6px' }} />{' '}
          Video
        </Button>
        <Button
          sx={(theme: Theme) => ({
            textTransform: 'capitalize',
            color: theme.palette.grey[600],
          })}
        >
          <CalendarMonthOutlinedIcon sx={{ fontSize: '1.5rem', mr: '6px' }} />{' '}
          Event
        </Button>
        <Button
          sx={(theme: Theme) => ({
            textTransform: 'capitalize',
            color: theme.palette.grey[600],
          })}
        >
          <ArticleOutlinedIcon sx={{ fontSize: '1.5rem', mr: '6px' }} /> Article
        </Button>
      </Box>
    </Paper>
  );
};
