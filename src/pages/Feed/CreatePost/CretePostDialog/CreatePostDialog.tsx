import { Button, Dialog, DialogActions, DialogContent, DialogTitle, IconButton, TextareaAutosize, Theme, Typography } from "@mui/material";

import CloseOutlinedIcon from '@mui/icons-material/CloseOutlined';
import { grey } from "@mui/material/colors";

interface ICreatePostDialog {
    open: boolean;
    onClose: () => void;
}

export const CreatePostDialog: React.FC<ICreatePostDialog> = ({ open, onClose }) => {
    return (
        <Dialog
            open={open}
            onClose={onClose}
            sx={(theme: Theme) => ({
                '& .MuiPaper-root': {
                    position: 'absolute',
                    top: '3.5rem',
                },
                '& .MuiDialogTitle-root': {
                    px: 2,
                    pt: 2,
                    pb: 1,
                },
                '& .MuiDialogContent-root': {
                    py: 1,
                    px: 2,
                },
                '& .MuiDialogActions-root': {
                    padding: theme.spacing(1),
                },
            })}
        >
            <DialogTitle
                sx={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                }}
            >
                <Typography fontSize={'1.3rem'}>Create a post</Typography>
                <IconButton
                    onClick={onClose}
                    sx={{
                        borderRadius: '4px',
                        p: '6px',
                    }}
                >
                    <CloseOutlinedIcon />
                </IconButton>
            </DialogTitle>
            <DialogContent>
                <TextareaAutosize
                    placeholder="what do you want to talk about"
                    style={{
                        width: '24rem',
                        height: '18rem',
                        fontFamily: 'inherit',
                        padding: "16px",
                        margin: 0,
                        fontSize: 'inherit',
                        outline: 'none',
                        border: `1px solid ${grey[300]}`,
                        borderRadius: '4px'
                    }}
                />
            </DialogContent>
            <DialogActions>
                <Button
                    disableElevation
                    variant="contained"
                    sx={{
                        textTransform: 'capitalize',
                    }}
                >
                    Post
                </Button>
            </DialogActions>
        </Dialog>
    )
}