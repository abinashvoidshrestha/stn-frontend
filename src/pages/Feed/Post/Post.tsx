import {
  Avatar,
  Box,
  Button,
  Collapse,
  Divider,
  Paper,
  styled,
  Theme,
  Typography,
} from '@mui/material';
import React, { useState } from 'react';

import ModeCommentOutlinedIcon from '@mui/icons-material/ModeCommentOutlined';
import ShareOutlinedIcon from '@mui/icons-material/ShareOutlined';
import ThumbUpOutlinedIcon from '@mui/icons-material/ThumbUpOutlined';

import { Comment } from './Comment/Comment';

import { IPost } from 'types';

const POST_HEADING_WORD_LIMIT = 100;

interface IPostComponent {
  post: IPost;
}

const PostImg = styled('img')(({ theme }) => ({
  objectFit: 'cover',
  width: '100%',
}));

interface ISmallPostIcon {
  icon: React.ReactNode;
  contentText: string;
}

const SmallPostIcon: React.FC<ISmallPostIcon> = ({ icon, contentText }) => {
  return (
    <Box
      sx={(theme: Theme) => ({
        display: 'flex',
        alignItems: 'center',
        color: theme.palette.grey[600],
        '& .MuiSvgIcon-root': {
          fontSize: '1rem',
        },
      })}
    >
      {icon}
      <Typography fontSize="0.8rem" sx={{ ml: '4px' }}>
        {contentText}
      </Typography>
    </Box>
  );
};

interface IPostActionIcon {
  icon: React.ReactNode;
  label: string;
  onClick?: React.MouseEventHandler<HTMLButtonElement> | undefined;
}

const PostActionIcon: React.FC<IPostActionIcon> = ({
  icon,
  label,
  onClick,
}) => {
  return (
    <Button
      onClick={onClick}
      sx={(theme: Theme) => ({
        color: theme.palette.grey[600],
        textTransform: 'capitalize',
        display: 'flex',
        gap: 1,
        px: 2,
        py: 1,
      })}
    >
      {icon}
      <Typography>{label}</Typography>
    </Button>
  );
};

export const Post: React.FC<IPostComponent> = ({ post }) => {
  const [seeMore, setSeeMore] = useState(false);
  const [isCommentVisible, setIsCommentVisible] = useState(false);

  return (
    <Paper
      key={post.id}
      elevation={1}
      sx={(theme: Theme) => ({
        width: '34rem',
        // border: "1px solid " + theme.palette.grey[300]
      })}
    >
      <Box sx={{ pt: 2, pb: 1 }}>
        <Box
          sx={{
            display: 'flex',
            alignItems: 'center',
            gap: 1,
            px: 2,
          }}
        >
          <Avatar
            {...(post.author.avatar && {
              src: post.author.avatar.src,
              alt: post.author.avatar.alt,
            })}
            sx={{
              height: '3.2rem',
              width: '3.2rem',
            }}
          >
            {post.author.name.split('')[0]}
          </Avatar>
          <Box>
            <Typography fontSize={'1rem'} fontWeight={'bold'}>
              {post.author.name}
            </Typography>
            <Typography fontSize={'0.9rem'} color={'GrayText'}>
              {post.author.headline} followers
            </Typography>
            <Typography
              fontSize={'0.9rem'}
              color={'GrayText'}
              sx={{ mt: '4px' }}
            >
              1 day ago
            </Typography>
          </Box>
        </Box>
        <Box sx={{ mt: 1 }}>
          <Typography variant={'body1'} fontSize="0.9rem" sx={{ px: 2 }}>
            {!seeMore && post.heading.length > POST_HEADING_WORD_LIMIT ? (
              <>
                {post.heading.slice(0, POST_HEADING_WORD_LIMIT)}{' '}
                <Typography
                  component={'span'}
                  fontSize="0.9rem"
                  color="GrayText"
                  sx={{
                    cursor: 'pointer',
                    ':hover': {
                      textDecoration: 'underline',
                    },
                  }}
                  onClick={() => setSeeMore(true)}
                >
                  see more...
                </Typography>
              </>
            ) : (
              post.heading
            )}
          </Typography>
          {post.bodyImg && (
            <Box
              sx={{
                mt: 1,
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <PostImg src={post.bodyImg.src} alt={post.bodyImg.alt} />
            </Box>
          )}
        </Box>
        <Box sx={{ px: 2, my: 1 }}>
          <Box sx={{ display: 'flex', gap: 1 }}>
            <SmallPostIcon
              icon={<ThumbUpOutlinedIcon />}
              contentText={'john doe and 6 other'}
            />
            <Box sx={{ flex: 1 }}></Box>
            <SmallPostIcon
              icon={<ModeCommentOutlinedIcon />}
              contentText={'5'}
            />
            <SmallPostIcon icon={<ShareOutlinedIcon />} contentText={'2'} />
          </Box>
        </Box>
        <Divider />
        <Box sx={{ px: 2 }}>
          <Box
            sx={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'space-between',
              mt: 1,
            }}
          >
            <PostActionIcon icon={<ThumbUpOutlinedIcon />} label={'Like'} />
            <PostActionIcon
              icon={<ModeCommentOutlinedIcon />}
              label={'Comment'}
              onClick={() => setIsCommentVisible(true)}
            />
            <PostActionIcon icon={<ShareOutlinedIcon />} label={'Share'} />
          </Box>
        </Box>
        <Collapse in={isCommentVisible}>
          <Comment post={post} />
        </Collapse>
      </Box>
    </Paper>
  );
};
