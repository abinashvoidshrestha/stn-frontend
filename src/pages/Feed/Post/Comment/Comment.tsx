import {
  Avatar,
  Box,
  Button,
  IconButton,
  Menu,
  MenuItem,
  Stack,
  styled,
  Theme,
  Typography,
} from '@mui/material';
import React from 'react';

import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import EmojiEmotionsOutlinedIcon from '@mui/icons-material/EmojiEmotionsOutlined';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import PhotoOutlinedIcon from '@mui/icons-material/PhotoOutlined';

import { comments } from 'data/data';
import { IComment, IPost } from 'types';

const CommentInput = styled('input')(({ theme }) => ({
  border: 'none',
  outline: 'none',
  fontSize: '1rem',
  height: '100%',
  width: '100%',
}));

interface ICommentItem {
  comment: IComment;
}

const CommentItem: React.FC<ICommentItem> = ({ comment }) => {
  return (
    <Box sx={{ display: 'flex', gap: 1, alignItems: 'top' }}>
      <Avatar
        {...(comment.author.avatar && {
          src: comment.author.avatar.src,
          alt: comment.author.avatar.alt,
        })}
        sx={{
          height: '2.6rem',
          width: '2.6rem',
          mt: '4px',
        }}
      >
        {comment.author.name.split('')[0]}
      </Avatar>
      <Box
        sx={(theme: Theme) => ({
          backgroundColor: theme.palette.grey[100],
          p: 1,
        })}
      >
        <Box
          sx={{
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'top',
          }}
        >
          <Box>
            <Typography fontSize={'0.9rem'} fontWeight="bold">
              {comment.author.name}
            </Typography>
            <Typography
              fontSize={'0.8rem'}
              color={'GrayText'}
              sx={{ mb: '6px' }}
            >
              {comment.author.headline} followers
            </Typography>
          </Box>
          <Box sx={{ display: 'flex' }}>
            <Typography
              fontSize={'0.8rem'}
              color={'GrayText'}
              sx={{ mb: '6px' }}
            >
              {comment.date}
            </Typography>
            <IconButton
              sx={{ p: 0, m: 0, height: 'fit-content', borderRadius: '4px' }}
            >
              <MoreVertIcon sx={{ mt: '2px', ml: '4px', fontSize: '1rem' }} />
            </IconButton>
          </Box>
        </Box>
        <Typography fontSize={'0.9rem'}>{comment.body}</Typography>
      </Box>
    </Box>
  );
};

interface ICommentComponent {
  post: IPost;
}

export const Comment: React.FC<ICommentComponent> = ({ post }) => {
  // ------ Comment Sort Menu ------

  const sortOptions = ['Most Relevant', 'Most Recent'];

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const [sortSelectedIndex, setSortSelectedIndex] = React.useState(1);
  const sortOpen = Boolean(anchorEl);

  const handleSortClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleSortMenuClick = (
    event: React.MouseEvent<HTMLElement>,
    index: number,
  ) => {
    setSortSelectedIndex(index);
    setAnchorEl(null);
  };

  const handleSortClose = () => {
    setAnchorEl(null);
  };

  const sortMenu = (
    <Menu
      id="sort-menu"
      anchorEl={anchorEl}
      open={sortOpen}
      onClose={handleSortClose}
    >
      {sortOptions.map((option, index) => (
        <MenuItem
          key={option}
          selected={index === sortSelectedIndex}
          onClick={(event) => handleSortMenuClick(event, index)}
        >
          {option}
        </MenuItem>
      ))}
    </Menu>
  );

  // ------------------------

  return (
    <Box sx={{ px: 2, mt: 1 }}>
      <Box sx={{ display: 'flex' }}>
        {/* TODO: separate component for Avatar */}
        <Avatar
          {...(post.author.avatar && {
            src: post.author.avatar.src,
            alt: post.author.avatar.alt,
          })}
          sx={{
            height: '2.6rem',
            width: '2.6rem',
          }}
        >
          {post.author.name.split('')[0]}
        </Avatar>
        <Box
          sx={(theme: Theme) => ({
            display: 'flex',
            alignItems: 'center',
            borderRadius: '4px',
            border: `1px solid ${theme.palette.grey[300]}`,
            overflow: 'hidden',
            ml: 1,
            px: 1,
            width: '100%',
          })}
        >
          <CommentInput placeholder="Add a comment" />
          <Box sx={{ display: 'flex' }}>
            <IconButton sx={{ borderRadius: '4px' }}>
              <EmojiEmotionsOutlinedIcon />
            </IconButton>
            <IconButton sx={{ borderRadius: '4px' }}>
              <PhotoOutlinedIcon />
            </IconButton>
          </Box>
        </Box>
      </Box>
      <Box sx={{ mt: 1 }}>
        <Button
          sx={(theme: Theme) => ({
            color: theme.palette.grey[600],
            textTransform: 'capitalize',
          })}
          endIcon={<ArrowDropDownIcon />}
          onClick={handleSortClick}
        >
          {sortOptions[sortSelectedIndex]}
        </Button>
        {sortMenu}
      </Box>
      <Stack sx={{ mt: 1 }} spacing={2}>
        {comments.map((comment) => (
          <CommentItem key={comment.id} comment={comment} />
        ))}
      </Stack>
    </Box>
  );
};
