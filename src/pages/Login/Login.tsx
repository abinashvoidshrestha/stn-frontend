import { zodResolver } from '@hookform/resolvers/zod';
import { Box, Button, Container, Grid, Link as MuiLink, Paper, Stack, styled, Typography } from "@mui/material";
import { FormProvider, SubmitHandler, useForm } from 'react-hook-form';
import { Link } from "react-router-dom";
import { object, string, TypeOf } from 'zod';

import { ReactComponent as AppleLogo } from 'assets/img/apple.svg';
import logo from 'assets/img/companyLogo.png';
import { ReactComponent as GoogleLogo } from 'assets/img/google.svg';

import TextInput from 'components/UI/Form/TextInput/TextInput';

export const LinkItem = styled(Link)`
  text-decoration: none;
  color: #3683dc;
  &:hover {
    text-decoration: underline;
    color: #5ea1b6;
  }
`;

// 👇 Styled Material UI Link Component
export const OauthMuiLink = styled(MuiLink)`
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #f5f6f7;
  border-radius: 4px;
  padding: 0.6rem 0;
  column-gap: 1rem;
  text-decoration: none;
  color: #393e45;
  font-weight: 500;
  cursor: pointer;

  &:hover {
    background-color: #fff;
    box-shadow: 0 1px 13px 0 rgb(0 0 0 / 15%);
  }

  & svg {
    width: 2rem;
  }
`;

const loginSchema = object({
  email: string().min(1, 'Email is required').email('Email is invalid'),
  password: string()
    .min(1, 'Password is required')
    .min(8, 'Password must be more than 8 characters')
    .max(32, 'Password must be less than 32 characters'),
});

type ILogin = TypeOf<typeof loginSchema>;

export const Login = () => {
  const defaultValues: ILogin = {
    email: '',
    password: '',
  };

  const methods = useForm<ILogin>({
    resolver: zodResolver(loginSchema),
    defaultValues,
  });

  const onSubmitHandler: SubmitHandler<ILogin> = (values: ILogin) => {
    console.log(values)
  };

  return (
    <Container
      maxWidth={false}
      sx={{ height: '100vh', backgroundColor: { xs: '#fff', md: '#f4f4f4' } }}
    >
      <Grid
        container
        justifyContent='center'
        alignItems='center'
        sx={{ width: '100%', height: '100%' }}
      >
        <Grid
          item
        >
          <Paper
            sx={{ maxWidth: '70rem', width: '100%', borderRadius: "4px" }}
          >
            <FormProvider {...methods}>
              <Grid
                container
                sx={{
                  py: '6rem',
                  px: '1rem',
                }}
              >
                <Grid
                  item
                  container
                  justifyContent='space-between'
                  rowSpacing={4}
                  sx={{
                    maxWidth: { sm: '45rem' },
                    marginInline: 'auto',
                  }}
                >
                  <Grid item xs={12} justifyContent={"center"}>
                    <Box
                      sx={{
                        height: '3rem',
                        width: 'fit-content',
                        mx: "auto",
                        mb: 2,
                        '& img': {
                          objectFit: 'cover',
                          height: '100%',
                        },
                      }}
                    >
                      <img src={logo} alt="company logo" />
                    </Box>
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={6}
                    sx={{ borderRight: { sm: '1px solid #ddd' } }}
                  >
                    <Box
                      display='flex'
                      flexDirection='column'
                      component='form'
                      noValidate
                      autoComplete='off'
                      sx={{ paddingRight: { sm: '3rem' } }}
                      onSubmit={methods.handleSubmit(onSubmitHandler)}
                    >
                      <Typography
                        variant='h6'
                        component='h1'
                        sx={{ textAlign: 'center', mb: '1.5rem' }}
                      >
                        Log into your account
                      </Typography>

                      <TextInput
                        label='Enter your email'
                        type='email'
                        name='email'
                        focused
                        required
                      />
                      <TextInput
                        type='password'
                        label='Password'
                        name='password'
                        required
                        focused
                      />
                      <Button
                        type='submit'
                        variant='contained'
                        sx={{
                          py: '0.8rem',
                          mt: 2,
                          width: '80%',
                          marginInline: 'auto',
                        }}
                      >
                        Login
                      </Button>

                    </Box>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <Typography
                      variant='h6'
                      component='p'
                      sx={{
                        paddingLeft: { sm: '3rem' },
                        mb: '1.5rem',
                        textAlign: 'center',
                      }}
                    >
                      Log in with another provider:
                    </Typography>
                    <Box
                      display='flex'
                      flexDirection='column'
                      sx={{ paddingLeft: { sm: '3rem' }, rowGap: '1rem' }}
                    >
                      <OauthMuiLink href=''>
                        <GoogleLogo />
                        Google
                      </OauthMuiLink>
                      <OauthMuiLink href=''>
                        <AppleLogo />
                        Apple
                      </OauthMuiLink>
                    </Box>
                  </Grid>
                </Grid>
                <Grid container justifyContent='center'>
                  <Stack sx={{ mt: '3rem', textAlign: 'center' }}>
                    <Typography sx={{ fontSize: '0.9rem', mb: '1rem' }}>
                      Need an account?{' '}
                      <LinkItem to='/signup'>Sign up here</LinkItem>
                    </Typography>
                    <Typography sx={{ fontSize: '0.9rem' }}>
                      Forgot your{' '}
                      <LinkItem to='/forgotPassword'>password?</LinkItem>
                    </Typography>
                  </Stack>
                </Grid>
              </Grid>
            </FormProvider>
          </Paper>
        </Grid>
      </Grid>
    </Container >
  );
};
