import { CssBaseline } from '@mui/material';
import { ThemeProvider } from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterLuxon } from '@mui/x-date-pickers/AdapterLuxon';
import { useRoutes } from 'react-router-dom';

import { routesClientLayout, routesNoLayout } from 'routes/routes';

import theme from 'theme';

function App() {
  const routes = useRoutes([...routesClientLayout, ...routesNoLayout]);

  return (
    <ThemeProvider theme={theme}>
      <LocalizationProvider dateAdapter={AdapterLuxon}>
        <CssBaseline />
        {routes}
      </LocalizationProvider>
    </ThemeProvider>
  );
}

export default App;
