import { Avatar, Box, Divider, Paper, Theme, Typography } from '@mui/material';
import React from 'react';

import { IUser } from 'types';

interface IUserSideCard {
  user: IUser;
  coverImg: {
    src: string;
    alt: string;
  };
}

export const UserSideCard: React.FC<IUserSideCard> = ({ user, coverImg }) => {
  return (
    <Paper
      elevation={1}
      sx={{
        width: '15rem',
        overflow: 'hidden',
        borderRadius: '4px',
        position: 'relative',
      }}
    >
      <Box
        sx={{
          height: '5rem',
          overflow: 'hidden',
          '& img': {
            objectFit: 'cover',
            width: '100%',
          },
        }}
      >
        <img src={coverImg.src} alt={coverImg.alt} />
      </Box>
      <Box
        sx={{
          position: 'absolute',
          top: '2.2rem',
          width: '100%',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <Avatar
          {...(user.avatar && {
            src: user.avatar.src,
            alt: user.avatar.alt,
          })}
          sx={(theme: Theme) => ({
            height: '5rem',
            width: '5rem',
            border: `2px solid ${theme.palette.common.white}`,
          })}
        >
          {user.name.split('')[0]}
        </Avatar>
      </Box>
      <Box sx={{ mt: '2.2rem', p: 1, textAlign: 'center' }}>
        <Typography fontSize={'1rem'} fontWeight={'bold'}>
          Welcome {user.name}
        </Typography>
        <Typography fontSize={'0.8rem'} color={'GrayText'}>
          {user.headline}
        </Typography>
      </Box>
      <Divider />
      <Box sx={{ px: 1, py: 2, textAlign: 'center' }}>
        <Typography fontWeight={'bold'} fontSize={'0.9rem'} color={'secondary'}>
          125
        </Typography>
        <Typography fontSize={'0.8rem'} color={'GrayText'}>
          people viewed your profile today
        </Typography>
      </Box>
      <Divider />
      <Box sx={{ p: 1, py: 2, textAlign: 'center' }}>
        <Typography fontWeight={'bold'} fontSize={'0.9rem'} color={'primary'}>
          23
        </Typography>
        <Typography fontSize={'0.9rem'} color={'GrayText'}>
          Connections
        </Typography>
        <Typography
          fontWeight={'bold'}
          fontSize={'0.9rem'}
          sx={{ textDecoration: 'underline' }}
        >
          Grow your network
        </Typography>
      </Box>
    </Paper>
  );
};
