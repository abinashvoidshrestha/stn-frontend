import { Box, Paper, Theme, Typography } from '@mui/material';
import { Stack } from '@mui/system';

import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import { users } from 'data/data';
import { AddToFeedItem } from './AddToFeedItem/AddToFeedItem';

export const AddToFeedCard = () => {
  return (
    <Paper sx={{ width: '18rem' }}>
      <Box sx={{ px: 2, pt: 2 }}>
        <Typography fontWeight={'bold'} fontSize={'1.1rem'}>
          Add to feed
        </Typography>
      </Box>
      <Stack sx={{ mt: 2, px: 2 }} spacing={1}>
        {users.map((feedItem, idx) => (
          // TODO: replace idx -> id
          <AddToFeedItem key={idx} feedItem={feedItem} />
        ))}
      </Stack>
      <Box
        sx={(theme: Theme) => ({
          p: 1,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          gap: 1,
          fontSize: '0.9rem',
          color: theme.palette.grey[600],
          cursor: 'pointer',
          ':hover': {
            backgroundColor: theme.palette.action.hover,
          },
        })}
      >
        Show more
        <ArrowForwardIcon sx={{ fontSize: '1.2rem' }} />
      </Box>
    </Paper>
  );
};
