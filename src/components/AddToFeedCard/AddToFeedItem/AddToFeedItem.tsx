import { Avatar, Box, Button, Divider, Typography } from '@mui/material';

import AddIcon from '@mui/icons-material/Add';
import { IUser } from 'types';

interface IAddToFeedItem {
  feedItem: IUser;
}

export const AddToFeedItem: React.FC<IAddToFeedItem> = ({ feedItem }) => (
  <>
    <Box sx={{ display: 'flex', alignItems: 'center', gap: 2, py: 1 }}>
      <Avatar
        {...(feedItem.avatar && {
          src: feedItem.avatar.src,
          alt: feedItem.avatar.alt,
        })}
        sx={{
          height: '4rem',
          width: '4rem',
        }}
      >
        {feedItem.name.split('')[0]}
      </Avatar>
      <Box>
        <Typography fontSize={'0.9rem'} fontWeight={'bold'}>
          {feedItem.name}
        </Typography>
        <Typography fontSize={'0.8rem'} color={'GrayText'}>
          {feedItem.headline}
        </Typography>
        <Button
          variant="outlined"
          size={'small'}
          sx={{
            mt: 1,
            py: '2px',
            px: '6px',
            textTransform: 'capitalize',
            borderRadius: '4px',
            fontSize: '0.8rem',
          }}
          endIcon={<AddIcon />}
        >
          Follow
        </Button>
      </Box>
    </Box>
    <Divider />
  </>
);
