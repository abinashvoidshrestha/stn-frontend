import { Box } from "@mui/material";
import { convertToRaw, EditorState as DraftEditorState } from "draft-js";
import { useState } from "react";
import { Editor, EditorState } from "react-draft-wysiwyg";

import "../../../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css";


export const WYSIWYG = () => {
    const [editorState, setEditorState] = useState(DraftEditorState.createEmpty())

    const handleEditorStateChange = (editorState: EditorState) => {
        setEditorState(editorState);
    };

    console.log(convertToRaw(editorState.getCurrentContent()));

    return (
        <Box>
            <Editor
                editorState={editorState}
                wrapperClassName="wysiwyg-wrapper"
                editorClassName="wysiwyg-editor"
                onEditorStateChange={handleEditorStateChange}
            />
        </Box>
    )
}
