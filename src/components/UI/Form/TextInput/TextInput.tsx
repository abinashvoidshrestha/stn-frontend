import { TextField, TextFieldProps } from '@mui/material';
import { FC } from 'react';
import { Controller, useFormContext } from 'react-hook-form';


// 👇 Type of Props the FormInput will receive
type FormInputProps = {
    name: string;
} & TextFieldProps;

const TextInput: FC<FormInputProps> = ({ name, ...otherProps }) => {
    // 👇 Utilizing useFormContext to have access to the form Context
    const {
        control,
        formState: { errors },
    } = useFormContext();

    return (
        <Controller
            control={control}
            name={name}
            defaultValue=''
            render={({ field }) => (
                <TextField
                    {...field}
                    {...otherProps}
                    variant='outlined'
                    sx={{ mb: '1.5rem' }}
                    error={!!errors[name]}
                    helperText={
                        errors[name] ? (errors[name]?.message as unknown as string) : ''
                    }
                />
            )}
        />
    );
};

export default TextInput;
