import { Box, Paper, Tab, Tabs, Typography } from '@mui/material';
import React, { useState } from 'react';

interface ITab {
  label: string;
  labelIcon?: React.ReactNode;
  tab: React.ReactNode;
}

interface ITabSwitcher {
  tabs: ITab[];
  centered?: boolean;
}

export const TabSwitcher: React.FC<ITabSwitcher> = ({
  tabs,
  centered = false,
}) => {
  const [value, setValue] = useState(0);

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  return (
    <Paper sx={{ pt: 1, px: 2, mb: 2 }}>
      <Tabs
        value={value}
        onChange={handleChange}
        sx={{ mb: 3 }}
        centered={centered}
      >
        {tabs.map((tab, idx) => (
          <Tab
            key={idx}
            label={
              <Box
                sx={{ display: 'inline-flex', alignItems: 'center', gap: 1 }}
              >
                {tab?.labelIcon}
                <Typography fontWeight={'bold'} fontSize={'0.9rem'}>
                  {tab.label}
                </Typography>
              </Box>
            }
          />
        ))}
      </Tabs>
      {tabs[value].tab}
    </Paper>
  );
};
