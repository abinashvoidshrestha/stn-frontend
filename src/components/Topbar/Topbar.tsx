import {
  Badge,
  BadgeProps,
  Box,
  Container,
  IconButton,
  Stack,
  styled,
  Theme,
} from '@mui/material';
import { Link, useLocation } from 'react-router-dom';

import ChatBubbleOutlineIcon from '@mui/icons-material/ChatBubbleOutline';
import HomeOutlinedIcon from '@mui/icons-material/HomeOutlined';
import NotificationsNoneIcon from '@mui/icons-material/NotificationsNone';
import PeopleAltOutlinedIcon from '@mui/icons-material/PeopleAltOutlined';
import WorkOutlineIcon from '@mui/icons-material/WorkOutline';

import { AccountMenu } from './AccountMenu/AccountMenu';
import { Search } from './Search/Search';

import logo from 'assets/img/companyLogo.png';

import { routeVars } from 'routes/routeVars';

const topbarActions = [
  {
    id: 'home',
    label: 'home',
    icon: <HomeOutlinedIcon />,
    url: routeVars.FEED,
    badgeContent: 0,
  },
  {
    id: 'jobs',
    label: 'find jobs',
    icon: <WorkOutlineIcon />,
    url: routeVars.JOBS,
    badgeContent: 0,
  },
  {
    id: 'connections',
    label: 'find connections',
    icon: <PeopleAltOutlinedIcon />,
    url: routeVars.CONNECTIONS,
    badgeContent: 0,
  },
  {
    id: 'chat',
    label: 'chat',
    icon: <ChatBubbleOutlineIcon />,
    url: routeVars.CHAT,
    badgeContent: 5,
  },
  {
    id: 'notifications',
    label: 'notifications',
    icon: <NotificationsNoneIcon />,
    url: routeVars.NOTIFICATIONS,
    badgeContent: 12,
  },
];

const StyledBadge = styled(Badge)<BadgeProps>(({ theme }) => ({
  '& .MuiBadge-badge': {
    fontSize: '0.7rem',
    fontWeight: 'bold',
  },
}));

export const Topbar = () => {
  const location = useLocation();

  return (
    <>
      <Box
        sx={(theme: Theme) => ({
          backgroundColor: theme.palette.primary.main,
          position: 'fixed',
          top: 0,
          left: 0,
          width: '100%',
          zIndex: theme.zIndex.appBar,
        })}
      >
        <Container
          sx={{
            display: 'flex',
            alignItems: 'center',
            height: '3.5rem',
          }}
        >
          <Box
            sx={{
              height: '2rem',
              '& img': {
                objectFit: 'cover',
                height: '100%',
              },
            }}
          >
            <img src={logo} alt="company logo" />
          </Box>
          <Box sx={{ ml: 3 }}>
            <Search />
          </Box>
          <Box sx={{ flex: 1 }}></Box>
          <Stack direction={'row'} gap={3}>
            <Stack direction={'row'} gap={1}>
              {topbarActions.map((action) => (
                <Link key={action.id} to={action.url}>
                  <IconButton
                    sx={(theme: Theme) => ({
                      p: '0.4rem',
                      borderRadius: '4px',
                      '& .MuiSvgIcon-root': {
                        color: theme.palette.common.white,
                        fontSize: '1.4rem',
                      },
                      ...(action.url === location.pathname
                        ? { backgroundColor: theme.palette.primary.light }
                        : {}),
                    })}
                  >
                    <StyledBadge
                      badgeContent={action?.badgeContent}
                      color={'error'}
                    >
                      {action.icon}
                    </StyledBadge>
                  </IconButton>
                </Link>
              ))}
            </Stack>
            <AccountMenu />
          </Stack>
        </Container>
      </Box>
    </>
  );
};
