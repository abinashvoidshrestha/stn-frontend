import {
  alpha,
  autocompleteClasses,
  Avatar,
  Backdrop,
  Box,
  Divider,
  Grow,
  Paper,
  styled,
  Theme,
  Typography,
  useAutocomplete,
} from '@mui/material';
import { deepOrange } from '@mui/material/colors';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

import SearchIcon from '@mui/icons-material/Search';

import { routeVars } from 'routes/routeVars';

// TODO: zIndex constant
const SEARCH_BAR_Z_IDX = 1500;
const SEARCH_WIDTH_PARTIAL = '14rem';
const SEARCH_WIDTH_COMPLETE = '26rem';

const SearchInput = styled('input')(({ theme }) => ({
  backgroundColor: 'transparent',
  border: 'none',
  outline: 'none',
  color: 'white',
  padding: '10px 0',
  overflow: 'hidden',
  '::placeholder': {
    color: alpha(theme.palette.common.white, 0.5),
  },
}));

const Listbox = styled('ul')(({ theme }) => ({
  padding: 0,
  margin: 0,
  [`& li.${autocompleteClasses.focused}`]: {
    backgroundColor: theme.palette.action.hover,
  },
}));

const ListboxItem = styled('li')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: '8px 10px',
  gap: '6px',
  ':hover': {
    backgroundColor: theme.palette.action.hover,
    cursor: 'pointer',
  },
}));

interface ISearchOption {
  label: string;
}

const searchList: ISearchOption[] = [
  { label: 'john doe' },
  { label: 'aragon ii elessar' },
  { label: 'harry potter' },
  { label: 'sam smith' },
  { label: 'chester bennington' },
];

// TODO: search as you type ref: https://mui.com/material-ui/react-autocomplete/#search-as-you-type
// TODO: clear input after search
export const Search = () => {
  const navigate = useNavigate();
  const [options, setOptions] = useState<ISearchOption[]>(searchList);
  const [isFocused, setIsFocused] = useState(false);

  const {
    getRootProps,
    getInputProps,
    getListboxProps,
    getOptionProps,
    groupedOptions,
    focused,
    value,
    inputValue,
  } = useAutocomplete({
    id: 'top-search-bar',
    options: options,
    getOptionLabel: (option) => option.label,
    isOptionEqualToValue: (option, value) => option.label === value.label,
  });

  useEffect(() => {
    setIsFocused(focused);
  }, [focused]);

  const handleSeeAll = () => {
    navigate(routeVars.SEARCH);
    setIsFocused(false);
  };

  return (
    <Box>
      <Backdrop open={isFocused} sx={{ zIndex: SEARCH_BAR_Z_IDX }} />
      <Box
        sx={(theme: Theme) => ({
          position: 'relative',
          height: 'fit-content',
          display: 'flex',
          alignItems: 'center',
          backgroundColor: theme.palette.primary.light,
          borderRadius: '4px',
          zIndex: SEARCH_BAR_Z_IDX + 1,
          pl: '8px',
          width: SEARCH_WIDTH_PARTIAL,
          border: 'solid 0px black',
          transition: `width 0.2s ease-in-out`,
          ...(isFocused
            ? {
                border: `2px solid ${theme.palette.primary.main}`,
                width: SEARCH_WIDTH_COMPLETE,
              }
            : {}),
        })}
        {...getRootProps()}
      >
        <SearchIcon
          sx={{
            mr: '4px',
            fontSize: '1.3rem',
            color: 'white',
          }}
        />
        <SearchInput placeholder="Search for jobs" {...getInputProps()} />
      </Box>
      {groupedOptions.length > 0 && (
        <Grow in={isFocused}>
          <Paper
            elevation={3}
            sx={{
              position: 'absolute',
              zIndex: SEARCH_BAR_Z_IDX + 1,
              mt: '4px',
              width: SEARCH_WIDTH_COMPLETE,
            }}
          >
            <Listbox {...getListboxProps()} sx={{ pt: 1 }}>
              {(groupedOptions as typeof searchList).map((option, index) => (
                <ListboxItem
                  key={index}
                  {...getOptionProps({ option, index })}
                  sx={{ px: 2, py: 1 }}
                >
                  <SearchIcon
                    sx={(theme: Theme) => ({
                      fontSize: '1.2rem',
                      color: theme.palette.grey[600],
                    })}
                  />
                  <Typography>{option.label}</Typography>
                  <Box sx={{ flex: 1 }}></Box>
                  <Avatar
                    sx={{ bgcolor: deepOrange[500], width: 26, height: 26 }}
                  >
                    N
                  </Avatar>
                </ListboxItem>
              ))}
              <Divider sx={{ pt: 1 }} />
              <Box
                onClick={handleSeeAll}
                sx={(theme: Theme) => ({
                  ':hover': {
                    backgroundColor: theme.palette.action.hover,
                  },
                })}
              >
                <Typography
                  sx={(theme: Theme) => ({
                    textAlign: 'center',
                    cursor: 'pointer',
                    py: 1,
                    color: theme.palette.grey[600],
                  })}
                >
                  See all results
                </Typography>
              </Box>
            </Listbox>
          </Paper>
        </Grow>
      )}
    </Box>
  );
};
