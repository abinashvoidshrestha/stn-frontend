import {
  Avatar,
  Box,
  Divider,
  IconButton,
  Menu,
  MenuItem,
  Typography,
} from '@mui/material';
import React from 'react';
import { useNavigate } from 'react-router-dom';

import { routeVars } from 'routes/routeVars';

const user = {
  name: 'Bilbo Baggins',
  headline: 'Ring Bearer at Middle Earth',
  avatar: {
    src: 'https://i.pravatar.cc',
    alt: 'dummy avatar',
  },
};

interface IMenuOption {
  label: string;
  url: string;
}

const menuOptions: IMenuOption[] = [
  { label: 'Settings & Privacy', url: '' },
  { label: 'Help', url: '' },
  { label: 'Post & Activities', url: '' },
];

export const AccountMenu = () => {
  const navigate = useNavigate();

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuOption = (option: IMenuOption) => {
    console.log(option);
    setAnchorEl(null);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleLogOut = () => {
    console.log('logging out');
    setAnchorEl(null);
  };

  const handleAccount = () => {
    console.log('account clicked');
    navigate(routeVars.USER);
    setAnchorEl(null);
  };

  const menu = (
    <Menu
      id="basic-menu"
      anchorEl={anchorEl}
      open={open}
      onClose={handleClose}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center',
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
      sx={{ mt: '4px' }}
    >
      <MenuItem onClick={handleAccount}>
        <Avatar
          sx={{
            height: '3rem',
            width: '3rem',
          }}
          src={user.avatar.src}
          alt={user.avatar.alt}
        >
          H
        </Avatar>
        <Box sx={{ ml: 2 }}>
          <Typography fontWeight={'bold'} fontSize={'1rem'}>
            {user.name}
          </Typography>
          <Typography fontSize={'0.9rem'} color={'GrayText'}>
            {user.headline}
          </Typography>
        </Box>
      </MenuItem>
      <Divider />
      {menuOptions.map((option, idx) => (
        <MenuItem key={idx} onClick={() => handleMenuOption(option)}>
          <Typography fontSize={'0.9rem'} color={'GrayText'}>
            {option.label}
          </Typography>
        </MenuItem>
      ))}

      <Divider />
      <MenuItem onClick={handleLogOut}>
        <Typography fontSize={'0.9rem'} color={'GrayText'}>
          Log Out
        </Typography>
      </MenuItem>
    </Menu>
  );

  return (
    <>
      <IconButton sx={{ p: 0 }} onClick={handleClick}>
        <Avatar
          sx={{
            height: open ? '2.2rem' : '2rem',
            width: open ? '2.2rem' : '2rem',
            transition: '0.1s linear',
            ':hover': {
              width: '2.2rem',
              height: '2.2rem',
            },
          }}
          src={user.avatar.src}
          alt={user.avatar.alt}
        >
          {user.name.split('')[0]}
        </Avatar>
      </IconButton>
      {menu}
    </>
  );
};
