import { Avatar, Box, Button, Theme, Typography } from '@mui/material';
import React from 'react';

import { IUser } from 'types';

import AddIcon from '@mui/icons-material/Add';

interface IPeopleCard {
  user: IUser;
}

export const PeopleCard: React.FC<IPeopleCard> = ({ user }) => {
  return (
    <Box
      sx={(theme: Theme) => ({
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        gap: 2,
        border: `1px solid ${theme.palette.divider}`,
        maxWidth: '15rem',
        px: 1,
        py: 2,
        borderRadius: '4px',
      })}
    >
      <Avatar
        {...(user.avatar && {
          src: user.avatar.src,
          alt: user.avatar.alt,
        })}
        sx={{
          height: '6rem',
          width: '6rem',
        }}
      >
        {user.name.split('')[0]}
      </Avatar>
      <Box sx={{ flex: 1, textAlign: 'center' }}>
        <Typography fontSize={'0.9rem'} fontWeight={'bold'}>
          {user.name}
        </Typography>
        <Typography fontSize={'0.8rem'} color={'GrayText'}>
          {user.headline} followers
        </Typography>
        <Typography fontSize={'0.8rem'} color={'GrayText'}>
          {user.location}
        </Typography>
      </Box>
      <Button
        variant="outlined"
        size={'small'}
        sx={{
          mt: 1,
          py: '4px',
          textTransform: 'capitalize',
          borderRadius: '4px',
          fontSize: '0.8rem',
        }}
        endIcon={<AddIcon />}
      >
        Follow
      </Button>
    </Box>
  );
};
