import { Avatar, Box, Theme, Typography } from '@mui/material';
import React from 'react';

import { IJob } from 'types';

interface IJobCard {
  job: IJob;
}

export const JobCard: React.FC<IJobCard> = ({ job }) => {
  return (
    <Box
      sx={(theme: Theme) => ({
        display: 'flex',
        alignItems: 'flex-start',
        gap: 2,
        borderRadius: '5px',
      })}
    >
      <Avatar
        src={job.company.logo.src}
        alt={job.company.logo.alt}
        sx={{
          height: '5.2rem',
          width: '5.2rem',
          borderRadius: '4px',
        }}
      >
        {job.company.name.split('')[0]}
      </Avatar>
      <Box sx={{ flex: 1 }}>
        <Typography fontSize={'1rem'} fontWeight={'bold'}>
          {job.title}
        </Typography>
        <Typography fontSize={'0.9rem'} color={'GrayText'}>
          {job.company.name}, {job.company.location}
        </Typography>
        <Typography fontSize={'0.8rem'} color={'GrayText'}>
          {job.description} followers
        </Typography>
        <Typography
          sx={{ display: 'inline-flex' }}
          fontSize={'0.8rem'}
          color={'GrayText'}
        >
          {job.date}
        </Typography>
        <Typography
          sx={{ display: 'inline-flex', ml: 1 }}
          fontSize={'0.8rem'}
          color={'secondary'}
        >
          {job.applicantCount} applicant
        </Typography>
      </Box>
    </Box>
  );
};
