import { Avatar, Box, Button, Typography } from '@mui/material';
import React from 'react';

import AddIcon from '@mui/icons-material/Add';

import { ICompany } from 'types';

interface ICompanyCard {
  company: ICompany;
}

export const CompanyCard: React.FC<ICompanyCard> = ({ company }) => {
  return (
    <Box sx={{ display: 'flex', alignItems: 'flex-start', gap: 2 }}>
      <Avatar
        src={company.logo.src}
        alt={company.logo.alt}
        sx={{
          height: '4.6rem',
          width: '4.6rem',
          borderRadius: '4px',
        }}
      >
        {company.name.split('')[0]}
      </Avatar>
      <Box sx={{ flex: 1 }}>
        <Typography fontSize={'0.9rem'} fontWeight={'bold'}>
          {company.name}
        </Typography>
        <Typography fontSize={'0.8rem'} color={'GrayText'}>
          {company.followers} followers
        </Typography>
        <Typography fontSize={'0.8rem'} color={'GrayText'}>
          {company.description}
        </Typography>
      </Box>
      <Button
        variant="outlined"
        size={'small'}
        sx={{
          mt: 1,
          py: '4px',
          textTransform: 'capitalize',
          borderRadius: '4px',
          fontSize: '0.8rem',
        }}
        endIcon={<AddIcon />}
      >
        Follow
      </Button>
    </Box>
  );
};
