import { Box, Container } from '@mui/material';
import { Outlet } from 'react-router-dom';

import { Topbar } from '../Topbar/Topbar';

export const Layout = (props: any) => {
  return (
    <Box>
      <Topbar />
      <Container sx={{ mt: 'calc(3.5rem + 1rem)' }}>
        <Outlet />
      </Container>
    </Box>
  );
};
